package com.example.android.rpapp.Fragment;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.example.android.rpapp.PageTransformAdapter.MessageSectionPageAdapter;
import com.example.android.rpapp.PageTransformationHelper.ZoomOutPageTransformer;
import com.example.android.rpapp.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class MessageFragment extends Fragment implements ViewPager.OnPageChangeListener  {

    private View messageView ;
    private MessageSectionPageAdapter messageSectionPageAdapter;

    private TabLayout messageTabLayout;
    private ViewPager messageViewPager;

    LinearLayout liveID;


    public MessageFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        messageView = inflater.inflate(R.layout.fragment_message, container, false);

        /*liveID = messageView.findViewById(R.id.liveID);
        liveID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent LBCIntent = new Intent(getActivity(), LiveBroadCastActivity.class);
                getActivity().startActivity(LBCIntent);
                getActivity().finish();
            }
        });*/


        messageSectionPageAdapter = new MessageSectionPageAdapter(getChildFragmentManager());
        messageViewPager = messageView.findViewById(R.id.news_frag_container);
        messageViewPager.setOffscreenPageLimit(messageSectionPageAdapter.getCount() - 1);
        messageViewPager.setPageTransformer(true, new ZoomOutPageTransformer());
        messageViewPager.setAdapter(messageSectionPageAdapter);

        messageTabLayout = messageView.findViewById(R.id.news_frag_tab);
        messageTabLayout.setupWithViewPager(messageViewPager);

        messageViewPager.addOnPageChangeListener(this);
        messageTabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(messageViewPager));



       return messageView;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) { }

    @Override
    public void onPageSelected(int position) {
        Fragment fragment = messageSectionPageAdapter.getFragment(position);
        if(fragment != null){
            fragment.onResume();
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) { }

}

