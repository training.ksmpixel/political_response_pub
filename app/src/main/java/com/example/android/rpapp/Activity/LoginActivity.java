package com.example.android.rpapp.Activity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.android.rpapp.Progress.Progress;
import com.example.android.rpapp.R;
import com.example.android.rpapp.Session_SharedPreference.Session;
import com.example.android.rpapp.Singleton.SingleTon;
import com.example.android.rpapp.Variables.StaticVariables;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView txtViewNewUser, txtViewForgotPassword;
    private EditText eTxtMobNumber, eTxtPassword;
    private Button btnLogin;

    Progress progress;
    Session session;

    Context mContext;
    private RequestQueue mQueue;
    Intent logIntent;
    String mobNum, pswd;

    String email = null,Voter_id_2 =null, occupation_2 =null, qualification_2 =null, Marital_staus_2=null, Blood_Group_2 =null,DOB_2 =null;

    String autoZero =null;

    String member_code = null;
    StaticVariables staticVariables;
    public static final int TIME_OUT = 3000;

    SharedPreferences logSharedPreferences;
    SharedPreferences.Editor logEditor;

    private String LOGIN_URL = staticVariables.LOGIN_API_FILE;
    private String PROFILE_UPDATE_URL = staticVariables.PROFILE_UPDATE_API_FILE;
    private String FORGOT_PASSWORD_URL = staticVariables.FORGOT_PASSWORD_API_FILE;

    AlertDialog.Builder dialogBuilder;

    private ArrayAdapter<CharSequence> arrayBG;
    private ArrayAdapter<CharSequence> arrayOP;
    private ArrayAdapter<CharSequence> arrayQualification;
    private ArrayAdapter<CharSequence> arrayMS;

    android.support.v7.app.ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

       /*actionBar = getSupportActionBar();
      actionBar.setTitle("LOGIN");
       getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(false);*/

        mContext = LoginActivity.this;

        staticVariables = new StaticVariables();
        logSharedPreferences = LoginActivity.this.getSharedPreferences(staticVariables.LOG_PREF, Context.MODE_PRIVATE);
        logEditor = logSharedPreferences.edit();
        session = new Session(LoginActivity.this);
        progress = new Progress();

        eTxtMobNumber = findViewById(R.id.log_edt_mob);
        eTxtPassword =findViewById(R.id.log_edt_pswd);
        btnLogin=findViewById(R.id.log_btn_login);

        txtViewForgotPassword=findViewById(R.id.log_txt_view_forgot);

        dialogBuilder = new AlertDialog.Builder(mContext);

        txtViewForgotPassword.setOnClickListener(this);
        btnLogin.setOnClickListener(this);
    }

    public void displayAlert(int code, String message) {
        dialogBuilder.setCancelable(false);
        if (code == 401) {
            dialogBuilder.setMessage(message);
            dialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
        }
        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
    }

    private void logCheck() {
        progress.Dialog(mContext, "Authenticating...");
        mobNum = eTxtMobNumber.getText().toString();
        pswd = eTxtPassword.getText().toString();

        if(mobNum.equals("") && pswd.equals("")){
            progress.progressDialog.dismiss();
            Toast.makeText(mContext, "Please fill the fields", Toast.LENGTH_LONG).show();
        } else if (!isValidMobile(mobNum)) {
            progress.progressDialog.dismiss();
            eTxtMobNumber.setError("Enter valid mobile number");
        } else if (!isValidPassword(pswd)) {
            progress.progressDialog.dismiss();
            Toast.makeText(LoginActivity.this, "Password must have 8 Characters", Toast.LENGTH_LONG).show();
        } else {
            final InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            assert imm != null;
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);

            mQueue = SingleTon.getInstance(mContext.getApplicationContext()).getRequestQueue();
            StringRequest stringRequest = new StringRequest(Request.Method.POST, LOGIN_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String JSONResponse = jsonObject.getString("response");
                            final JSONObject jsonObjResponse = new JSONObject(JSONResponse);
                            final String code = jsonObjResponse.getString("code");
                            final String message = jsonObjResponse.getString("message");
                            String result = jsonObjResponse.getString("result");


                            if (Integer.parseInt(code) == 200){

                               JSONObject jsonObjResult = new JSONObject(result);
                               final String vote_id = jsonObjResult.getString("intVoter_Id");
                                final String login_member_id = jsonObjResult.getString("member_id");
                                final String login_user_dp = jsonObjResult.getString("userImage");
                                final String login_user_aadhar = jsonObjResult.getString("varAadharnumber");
                                final String login_user_name = jsonObjResult.getString("varName");
                                final String login_user_mob_no = jsonObjResult.getString("varMobile");
                                final String login_user_refernce_num = jsonObjResult.getString("varReference_num");
                                final String login_user_ac = jsonObjResult.getString("varAc");
                                final String login_user_ward_no = jsonObjResult.getString("intWard_num");
                                final String login_user_sub_dist = jsonObjResult.getString("varSub_dist");
                                final String login_user_dist = jsonObjResult.getString("varDist");
                                final String login_user_area = jsonObjResult.getString("varArea");
                                final String login_user_gender = jsonObjResult.getString("varGender");
                                final String login_user_pincode = jsonObjResult.getString("intPincode");
                                final String login_user_reg_date = jsonObjResult.getString("dteReg_date");
                                final String login_user_position = jsonObjResult.getString("varMember");
                                final String login_user_refernced_by = jsonObjResult.getString("varRefered_by");
                                final String login_user_age = jsonObjResult.getString("varAge");
                                final String login_user_occupation = jsonObjResult.getString("varOccupation");
                                final String login_user_blood_group = jsonObjResult.getString("varBlood_group");
                                final String login_user_marital_status = jsonObjResult.getString("varMarital_status");
                                final String login_user_qualification = jsonObjResult.getString("varQualification");
                                final String login_user_state = jsonObjResult.getString("varState");
                                final String dateOfBirth = jsonObjResult.getString("varDOB");
                                final String postionCode = jsonObjResult.getString("varPositionCode");
                                final String distCode = jsonObjResult.getString("varDistCode");
                                final String acCode = jsonObjResult.getString("varConsttuencyCode");
                                final String autoGen = jsonObjResult.getString("intRegisterId");
                                final String email = jsonObjResult.getString("varEmail");
                                final String cname = jsonObjResult.getString("varChat_name");
                                final String level = jsonObjResult.getString("level");

                                int i = autoGen.length();

                                switch (i){
                                    case 1:
                                        autoZero = "0000000";
                                        break;
                                    case 2:
                                        autoZero = "000000";
                                        break;
                                    case 3:
                                        autoZero = "00000";
                                        break;
                                    case 4:
                                        autoZero = "0000";
                                        break;
                                    case 5:
                                        autoZero = "000";
                                        break;
                                    case 6:
                                        autoZero = "00";
                                        break;
                                    case 7:
                                        autoZero = "0";
                                        break;
                                    default:
                                        autoZero = "";
                                        break;
                                }
                                member_code = postionCode+distCode+acCode+autoZero+autoGen;

                                logEditor.putString(staticVariables.KEY_USER_EMAIL, email);
                                logEditor.putString(staticVariables.KEY_USER_CHAT_NAME, cname);
                                logEditor.putString(staticVariables.KEY_USER_LEVEL, level);
                                logEditor.putString(staticVariables.KEY_USER_MEMBER_ID, login_member_id);
                                logEditor.putString(staticVariables.KEY_USER_DP, login_user_dp);
                                logEditor.putString(staticVariables.KEY_USER_AADHAR_NO, login_user_aadhar);
                                logEditor.putString(staticVariables.KEY_USER_NAME, login_user_name);
                                logEditor.putString(staticVariables.KEY_USER_MOBILE_NO, login_user_mob_no);
                                logEditor.putString(staticVariables.KEY_USER_REFERENCE_NO, login_user_refernce_num);
                                logEditor.putString(staticVariables.KEY_USER_AC, login_user_ac);
                                logEditor.putString(staticVariables.KEY_USER_WARD_NO, login_user_ward_no);
                                logEditor.putString(staticVariables.KEY_USER_SUB_DIST, login_user_sub_dist);
                                logEditor.putString(staticVariables.KEY_USER_DIST, login_user_dist);
                                logEditor.putString(staticVariables.KEY_USER_AREA, login_user_area);
                                logEditor.putString(staticVariables.KEY_USER_GENDER, login_user_gender);
                                logEditor.putString(staticVariables.KEY_USER_PINCODE, login_user_pincode);
                                logEditor.putString(staticVariables.KEY_USER_REGISTRATION_DATE, login_user_reg_date);
                                logEditor.putString(staticVariables.KEY_USER_MEMBER_POSITION, login_user_position);
                                logEditor.putString(staticVariables.KEY_USER_REFERED_BY, login_user_refernced_by);
                                logEditor.putString(staticVariables.KEY_USER_AGE, login_user_age);
                                logEditor.putString(staticVariables.KEY_USER_VOTER_ID, vote_id);
                                logEditor.putString(staticVariables.KEY_USER_OCCUPATION, login_user_occupation);
                                logEditor.putString(staticVariables.KEY_USER_BLOOD_GROUP, login_user_blood_group);
                                logEditor.putString(staticVariables.KEY_USER_MARITAL_STATUS, login_user_marital_status);
                                logEditor.putString(staticVariables.KEY_USER_QUALIFICATION, login_user_qualification);
                                logEditor.putString(staticVariables.KEY_USER_STATE, login_user_state);
                                logEditor.putString(staticVariables.KEY_USER_DOB, dateOfBirth);
                                logEditor.commit();

                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        progress.progressDialog.dismiss();

                                            if(vote_id.equals("0")){
                                                Toast.makeText(LoginActivity.this, "Please Fill All the Details", Toast.LENGTH_LONG).show();
                                                voter_details();
                                            }else{
                                                openMain();
                                            }
                                    }
                                },TIME_OUT);
                            } else {
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        progress.progressDialog.dismiss();
                                        displayAlert(Integer.parseInt(code),message);
                                    }
                                },TIME_OUT);
                            }
                        } catch (final Exception e) {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    progress.progressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), String.valueOf(e), Toast.LENGTH_LONG).show();
                                }
                            },TIME_OUT);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(final VolleyError error) {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                progress.progressDialog.dismiss();
                                Toast.makeText(getApplicationContext(), String.valueOf(error), Toast.LENGTH_LONG).show();
                            }
                        },TIME_OUT);
                    }
                }){
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("mobile", mobNum);
                    params.put("password", pswd);
                   // params.put("member_code",member_code);
                    return params;
                }
            };
            mQueue.add(stringRequest);
        }
    }

    private String imageToString(Bitmap bitmap){
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG,10, byteArrayOutputStream);
        byte[] imgBytes = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(imgBytes, Base64.DEFAULT);
    }

    public void voter_details(){

        final Dialog dialog = new Dialog(this);
        dialog.setCancelable(false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_voter_id_details);

        ImageView dismiss_btn = dialog.findViewById(R.id.dlg_voter_details_icon_dismiss);
        Button Proceedbtn = dialog.findViewById(R.id.voter_details_checking_btn);

        dismiss_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Toast.makeText(LoginActivity.this,"Please Fill all Details", Toast.LENGTH_SHORT).show();
            }
        });

        final EditText voter_id = dialog.findViewById(R.id.reg_voter_id) ;
        final EditText user_email = dialog.findViewById(R.id.reg_email_id) ;
        final Spinner occupation = dialog.findViewById(R.id.reg_occupation);
        RelativeLayout SpinnerBgLayout =dialog.findViewById(R.id.reg_lay_user_blood_grp);
        RelativeLayout SpinnerMaritalStatus=dialog.findViewById(R.id.reg_lay_user_Marital_status);
        RelativeLayout SpinnerQualification=dialog.findViewById(R.id.reg_lay_spinner_qualification);
        final TextView DOB = dialog.findViewById(R.id.reg_dob);


        DOB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Calendar myCalendar = Calendar.getInstance();
                DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        // TODO Auto-generated method stub
                        myCalendar.set(Calendar.YEAR, year);
                        myCalendar.set(Calendar.MONTH, monthOfYear);
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        String myFormat = "dd MM yyyy"; // your format
                        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());

                        DOB.setText(sdf.format(myCalendar.getTime()));
                    }

                };
                new DatePickerDialog(mContext, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();

            }
        });

        final Spinner spinnerBg=dialog.findViewById(R.id.reg_user_blood_grp);
        final Spinner spinnerMS=dialog.findViewById(R.id.reg_user_Marital_status);
        final Spinner spinnerQulaification=dialog.findViewById(R.id.reg_spinner_qualification);

        arrayOP = ArrayAdapter.createFromResource(this, R.array.occupation, android.R.layout.simple_spinner_item);
        arrayOP.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        occupation.setAdapter(arrayOP);

        arrayBG = ArrayAdapter.createFromResource(this, R.array.blood_group, android.R.layout.simple_spinner_item);
        arrayBG.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerBg.setAdapter(arrayBG);

        arrayQualification = ArrayAdapter.createFromResource(this, R.array.qualification, android.R.layout.simple_spinner_item);
        arrayQualification.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerQulaification.setAdapter(arrayQualification);

        arrayMS = ArrayAdapter.createFromResource(this, R.array.marital_status, android.R.layout.simple_spinner_item);
        arrayMS.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerMS.setAdapter(arrayMS);

        dialog.show();

        Proceedbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progress.Dialog(mContext, "Saving...");
                Voter_id_2 = voter_id.getText().toString();
                email = user_email.getText().toString();
                occupation_2  = occupation.getSelectedItem().toString();
                Blood_Group_2 = spinnerBg.getSelectedItem().toString();
                qualification_2=spinnerQulaification.getSelectedItem().toString();
                Marital_staus_2 = spinnerMS.getSelectedItem().toString();
                DOB_2 =DOB.getText().toString();

                if(Voter_id_2.equals("")){
                    progress.progressDialog.dismiss();
                    Toast.makeText(mContext, "Please Enter Your Voter ID", Toast.LENGTH_LONG).show();
                }else if(email.equals("")){
                    progress.progressDialog.dismiss();
                    Toast.makeText(mContext, "Please Enter Your Email ID", Toast.LENGTH_LONG).show();
                } else if(!isValidEmail(email)){
                    progress.progressDialog.dismiss();
                    Toast.makeText(mContext, "Enter Valid Email", Toast.LENGTH_LONG).show();
                }else if(occupation_2.equals("--Select your Occupation--")){
                    Toast.makeText(mContext, "Please Select Your Occupation", Toast.LENGTH_LONG).show();
                } else if(DOB_2.equals("")){
                    Toast.makeText(mContext, "Please Enter Your DOB", Toast.LENGTH_LONG).show();
                } else if(Blood_Group_2.equals("--Select Blood Group--")){
                    progress.progressDialog.dismiss();
                    Toast.makeText(LoginActivity.this, "Please select your Blood Group", Toast.LENGTH_LONG).show();
                } else if(qualification_2.equals("--Select Qualification--")){
                    progress.progressDialog.dismiss();
                    Toast.makeText(LoginActivity.this, "Please select your Qualification", Toast.LENGTH_LONG).show();
                } else if(Marital_staus_2.equals("--Select Marital Status--")){
                    progress.progressDialog.dismiss();
                    Toast.makeText(LoginActivity.this, "Please select your MaritalStatus", Toast.LENGTH_LONG).show();
                }else {
                    final InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);

                    mQueue = SingleTon.getInstance(mContext.getApplicationContext()).getRequestQueue();
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, PROFILE_UPDATE_URL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    String JSONResponse = jsonObject.getString("response");
                                    JSONObject jsonObjResponse = new JSONObject(JSONResponse);
                                    final String code = jsonObjResponse.getString("code");
                                    final String message = jsonObjResponse.getString("message");
                                    String result = jsonObjResponse.getString("result");
                                    Log.e("Dialog Profile Update",""+response);
                                    if (Integer.parseInt(code) == 201) {

                                        new Handler().postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                progress.progressDialog.dismiss();
                                                dialog.dismiss();
                                                Log.e("","dialog dismiss");
                                                openMain();
                                            }
                                        }, TIME_OUT);
                                    } else {
                                        new Handler().postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                progress.progressDialog.dismiss();
                                                displayAlert(Integer.parseInt(code), message);
                                            }
                                        }, TIME_OUT);
                                    }
                                } catch (final Exception e) {
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            progress.progressDialog.dismiss();
                                            Toast.makeText(getApplicationContext(), String.valueOf(e), Toast.LENGTH_LONG).show();
                                        }
                                    }, TIME_OUT);
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(final VolleyError error) {
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        progress.progressDialog.dismiss();
                                        Toast.makeText(getApplicationContext(), String.valueOf(error), Toast.LENGTH_LONG).show();
                                    }
                                }, TIME_OUT);
                            }
                        }) {
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> params = new HashMap<>();
                            params.put("mob",mobNum);
                            params.put("email",email);
                            params.put("voter_id", Voter_id_2);
                            params.put("occupation", occupation_2);
                            params.put("blood_group", Blood_Group_2);
                            params.put("marital_status", Marital_staus_2);
                            params.put("qualification", qualification_2);
                            params.put("dob", DOB_2);
                            params.put("member_code",member_code);

                            logEditor.putString(staticVariables.KEY_USER_VOTER_ID, Voter_id_2);
                            logEditor.putString(staticVariables.KEY_USER_EMAIL, email);
                            logEditor.putString(staticVariables.KEY_USER_OCCUPATION, occupation_2);
                            logEditor.putString(staticVariables.KEY_USER_BLOOD_GROUP, Blood_Group_2);
                            logEditor.putString(staticVariables.KEY_USER_MARITAL_STATUS, Marital_staus_2);
                            logEditor.putString(staticVariables.KEY_USER_QUALIFICATION, qualification_2);
                            logEditor.putString(staticVariables.KEY_USER_DOB, DOB_2);
                            logEditor.putString(staticVariables.KEY_USER_MEMBER_ID, member_code);
                            logEditor.commit();

                            return params;
                        }
                    };
                    mQueue.add(stringRequest);
                    dialog.show();
                }
            }
        });
    }

    public void openMain(){
        session.setLoggedIn(true);
        logIntent = new Intent(mContext, MainActivity.class);
        logIntent.putExtra("viewpager_position", 0);
        startActivity(logIntent);
        finish();
    }

    private boolean isValidPassword(String pass) {
        if (pass != null && pass.length() >= 8) {
            return true;
        }
        return false;
    }

    private boolean isValidMobile(String mob){
        if(mob != null && mob.length() == 10){
            return true;
        }
        return false;
    }
    private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.log_btn_login:
                logCheck();
                break;

            case  R.id.log_txt_view_forgot:
                forgotPassword();
                break;
        }
    }


    public void forgotPassword(){
        final Dialog dialog = new Dialog(mContext);
        dialog.setCancelable(false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_content_forgot_password);

        final EditText editTextEmail = dialog.findViewById(R.id.login_forgot_dlg_email_edt);
        final Button btnFPProceed = dialog.findViewById(R.id.login_forgot_dlg_btn_proceed);
        final ImageButton imgBtnDismiss = dialog.findViewById(R.id.login_forgot_dlg_icon_dismiss);

        imgBtnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        btnFPProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progress.Dialog(mContext, "Authenticating...");
                final String getEmail = editTextEmail.getText().toString();
                if(!isValidEmail(getEmail)){
                    progress.progressDialog.dismiss();
                    Toast.makeText(mContext, "Enter Valid Email", Toast.LENGTH_LONG).show();
                }
                else {
                    final InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                    mQueue = SingleTon.getInstance(getApplicationContext()).getRequestQueue();

                    StringRequest stringRequest = new StringRequest(Request.Method.POST, FORGOT_PASSWORD_URL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    String JSONResponse = jsonObject.getString("response");
                                    JSONObject jsonObjResponse = new JSONObject(JSONResponse);
                                    final String code = jsonObjResponse.getString("code");
                                    final String message = jsonObjResponse.getString("message");
                                    String result = jsonObjResponse.getString("result");
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            dialog.dismiss();
                                            progress.progressDialog.dismiss();
                                            Toast.makeText(getApplicationContext(),message, Toast.LENGTH_LONG).show();
                                        }
                                    },TIME_OUT);
                                } catch (final JSONException e){
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            progress.progressDialog.dismiss();
                                            Toast.makeText(mContext, String.valueOf(e), Toast.LENGTH_LONG).show();
                                        }
                                    },TIME_OUT);
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(final VolleyError error) {
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        progress.progressDialog.dismiss();
                                        Toast.makeText(mContext, String.valueOf(error), Toast.LENGTH_LONG).show();
                                    }
                                },TIME_OUT);
                            }
                        }){
                        protected Map<String, String> getParams() throws AuthFailureError{
                            Map<String, String> params = new HashMap<>();
                            params.put("EMail", getEmail);
                            return params;
                        }
                    };
                    mQueue.add(stringRequest);
                }
            }
        });
        dialog.show();
    }
}
