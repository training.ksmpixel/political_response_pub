package com.example.android.rpapp.Variables;


public class StaticVariables {

    public static final String API_URL_PATH = "http://ksmps.com.md-in-30.webhostbox.net/political/";

    public static final String PROFILE_FOLDER = API_URL_PATH+"profile_images/";
    public static final String FEED_FOLDER = API_URL_PATH+"feed_images/";
    public static final String FEED_VIDEOS_PATH = API_URL_PATH+"feed_videos/";

    public static final String NEWS_IMAGE_FOLDER = API_URL_PATH+"news_images/";

    public static final String DP_CHANGER_API_FILE = API_URL_PATH+"image_update.php";

    public static final String REFERENCE_USER_VERIFY = API_URL_PATH+"checkReference.php";


    public static final String REGISTER_API_FILE = API_URL_PATH+"register.php";
    public static final String LOGIN_API_FILE = API_URL_PATH+"login.php";
    public static final String PROFILE_UPDATE_API_FILE = API_URL_PATH+"profile_update.php";
    public static final String PERSONAL_DETAIL_UPDATE = API_URL_PATH+"profile_update3.php";
    public static final String PERSONAL_VOTER_DETAIL_UPDATE = API_URL_PATH+"profile_update2.php";

    public static final String FORGOT_PASSWORD_API_FILE = API_URL_PATH+"mail_password.php";
    public static final String CHANGE_PASSWORD_API_FILE = API_URL_PATH+"change_password.php";

    public static final String COMPLAINT_URL = API_URL_PATH+"complaints.php";
    public static final String COM_RES_URL = API_URL_PATH+"complaint_update.php";

    public static final String ALL_COMPLAINT_URL = API_URL_PATH+"showall_complaints.php";
    public static final String COMPLAINT_VIEW_URL = API_URL_PATH+"complaints_view.php";

    //news
    public static final String NEWS_URL = API_URL_PATH+"showall_news.php";


    //FEED
    public static final String FEED_LIST = API_URL_PATH+"show_allfeeds.php";
    public static final String DEL_FEED = API_URL_PATH+"delete_feed.php";
    public static final String HO_FEED_URL = API_URL_PATH+"feed_ac_type.php";
    public static final String AC_FEED_URL = API_URL_PATH+"feed_ac.php";
    public static final String FEED_TEXT_POST_URL = API_URL_PATH+"feed.php";
    public static final String FEED_IMG_POST_URL = API_URL_PATH+"feed.php";
    public static final String FEED_LIKE = API_URL_PATH+"like.php";
    public static final String FEED_LIKES_VIEW = API_URL_PATH+"view_likes.php";

    public static final String FEED_VIDEO_UPLOAD = API_URL_PATH+"video.php";
    public static final String TREE_MEMBER_VIEW = API_URL_PATH+".php";
    public static final String MY_CLIENT_LIST_URL = API_URL_PATH+"client_members.php";
    public static final String ALL_CLIENT_LIST_URL = API_URL_PATH+"showall_members.php";
    public static final String CREATE_NEW_CHAT = API_URL_PATH+"single_chat.php";


    //Chat
    public static final String CHAT_MESSAGE_LIST = API_URL_PATH+"chat_display.php";
    public static final String CHAT_NEW_MESSAGE = API_URL_PATH+"chat.php";



    public static final String LOG_PREF = "logPref";

    public static final String KEY_USER_MEMBER_ID = "USER_MEMBER_ID";
    public static final String KEY_USER_DP = "USER_DP";
    public static final String KEY_USER_AADHAR_NO = "USER_AADHAR_NO";
    public static final String KEY_USER_NAME = "USER_NAME";
    public static final String KEY_USER_MOBILE_NO = "USER_MOBILE_NUMBER";
    public static final String KEY_USER_REFERENCE_NO = "USER_REFERENCE_NO";
    public static final String KEY_USER_AC = "USER_AC";
    public static final String KEY_USER_WARD_NO = "USER_WARD_NO";
    public static final String KEY_USER_SUB_DIST = "USER_SUB_DIST";
    public static final String KEY_USER_DIST = "USER_DIST";
    public static final String KEY_USER_AREA = "USER_AREA";
    public static final String KEY_USER_GENDER = "USER_GENDER";
    public static final String KEY_USER_PINCODE = "USER_PIN_CODE";
    public static final String KEY_USER_REGISTRATION_DATE = "USER_REGISTRATION_DATE";
    public static final String KEY_USER_MEMBER_POSITION = "USER_MEMBER_POSITION";
    public static final String KEY_USER_REFERED_BY = "USER_REFERD_BY";
    public static final String KEY_USER_AGE = "USER_AGE";
    public static final String KEY_USER_VOTER_ID = "USER_VOTER_ID";
    public static final String KEY_USER_OCCUPATION = "USER_OCCUPATION";
    public static final String KEY_USER_BLOOD_GROUP = "USER_BLOOD_GROUP";
    public static final String KEY_USER_MARITAL_STATUS = "USER_MARITAL_STATUS";
    public static final String KEY_USER_QUALIFICATION = "USER_QUALIFICATION";
    public static final String KEY_USER_DOB = "DD-MM-YYYY";
    public static final String KEY_USER_STATE = "STATE";
    public static final String KEY_USER_EMAIL = "EMAIL";
    public static final String KEY_USER_CHAT_NAME = "CNAME";
    public static final String KEY_USER_LEVEL = "LEVEL";


    //Chat
    public static final String BASE_URL ="http://ksmps.com.md-in-30.webhostbox.net/political/gcm_chat/v1";

    public static final String LOGIN = BASE_URL + "/user/login";
    public static final String USER = BASE_URL + "/user/_ID_";
    public static final String CHAT_ROOMS = BASE_URL + "/chat_rooms";
    public static final String CHAT_THREAD = BASE_URL + "/chat_rooms/_ID_";
    public static final String CHAT_ROOM_MESSAGE = BASE_URL + "/chat_rooms/_ID_/message";

    //team chat url
    public static final String CHAT_TEAMS = BASE_URL + "/chat_rooms1";
    public static final String CHAT_THREAD1 = BASE_URL + "/chat_rooms1/_ID_";
    public static final String CHAT_ROOM_MESSAGE1 = BASE_URL + "/chat_rooms1/_ID_/message";



    //Replace

    public static final String KEY_DIST = "USER_DIST";
    public static final String KEY_TEAM_ID = "USER_TEAM_ID";
    public static final String KEY_TEAM_NAME = "USER_TEAM_NAME";
    public static final String KEY_DIS_ID = "USER_DIS_ID";
    public static final String KEY_NOTIFICATION_STATUS = "NOTIFY_STATUS";
    public static final String KEY_NOTIFICATION_RINGTONE = "NOTIFY_RING";


}