package com.example.android.rpapp.Activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.android.rpapp.PageTransformAdapter.MainSectionsPageAdapter;
import com.example.android.rpapp.PageTransformationHelper.DepthPageTransformer;
import com.example.android.rpapp.R;
import com.example.android.rpapp.Session_SharedPreference.Session;
import com.example.android.rpapp.Settings.SettingsActivity;
import com.example.android.rpapp.Variables.StaticVariables;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener ,NavigationView.OnNavigationItemSelectedListener {

    TabLayout tabLayout;
    View menuView;

    String inviteContent;

    boolean doubleBackToExitPressedOnce = true;


    TextView LogoutTxtView,txtViewUserSettings,txtViewUserContactUs,txtViewUserInvite,txtInvitaion;

    private MainSectionsPageAdapter mainSectionPageAdapter;
    private ViewPager mViewPager;

    Session session;
    StaticVariables staticVariables;

    SharedPreferences mainSharedPreference;
    SharedPreferences.Editor mainEditor;

    TextView Name,MemberId;

    private ArrayList<Integer> listDrawables = new ArrayList<Integer>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        listDrawables.add(R.drawable.ic_about);
        listDrawables.add(R.drawable.ic_contract);
        listDrawables.add(R.drawable.ic_menu);


        staticVariables = new StaticVariables();
        session = new Session(this);
        if (!session.loggedIn()) {
            logout();
        }

        int position = 0;
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            position = extras.getInt("viewpager_position");
        }


        mainSharedPreference = MainActivity.this.getSharedPreferences(staticVariables.LOG_PREF, Context.MODE_PRIVATE);
        mainEditor = mainSharedPreference.edit();

        mainSectionPageAdapter = new MainSectionsPageAdapter(getSupportFragmentManager());

        mViewPager = findViewById(R.id.main_container);

        mViewPager.setOffscreenPageLimit(mainSectionPageAdapter.getCount() - 1);
        mViewPager.setAdapter(mainSectionPageAdapter);
        mViewPager.setPageTransformer(true, new DepthPageTransformer());
        mViewPager.setCurrentItem(position);

        tabLayout = findViewById(R.id.main_tabs);
        tabLayout.setupWithViewPager(mViewPager);

        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            tabLayout.getTabAt(i).setIcon(listDrawables.get(i));
        }
        mViewPager.addOnPageChangeListener(this);
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        View header=navigationView.getHeaderView(0);
        /*View view=navigationView.inflateHeaderView(R.layout.nav_header_main);*/
        Name = (TextView)header.findViewById(R.id.user_name);
        MemberId = (TextView)header.findViewById(R.id.register_id);
        Name.setText(mainSharedPreference.getString(staticVariables.KEY_USER_NAME,"null"));
        MemberId.setText(mainSharedPreference.getString(staticVariables.KEY_USER_MEMBER_ID,"null"));


       }
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.txt_user_invite) {
           // Toast.makeText(this,"Invite",Toast.LENGTH_LONG).show();
            invite();
            item.setChecked(true); }
            else if (id == R.id.txt_user_settings) {
            Intent settingsIntent = new Intent(this, SettingsActivity.class);
            startActivity(settingsIntent);
        } else if (id == R.id.txt_user_contact_us) {
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", "+91 999 464 7183", null));
            startActivity(intent);
        } else if (id == R.id.logoutTxtView) {
            logout();
        }


            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }



    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
            }
        else if (mViewPager.getCurrentItem() != 0) {
                    mViewPager.setCurrentItem(0, false);
                } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setMessage("Do you want to exit application?")
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                    overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    }


            }





    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_main3_drawer, menu);
        txtViewUserSettings=menuView.findViewById(R.id.txt_user_settings);
        txtViewUserInvite=menuView.findViewById(R.id.txt_user_invite);
        txtViewUserContactUs=menuView.findViewById(R.id.txt_user_contact_us);
        LogoutTxtView = menuView.findViewById(R.id.logoutTxtView);
        //txtInvitaion=menuView.findViewById(R.id.invitation_content);

      inviteContent  =("\t \t  AMMKMESSAGE APP \n \n Hey there, this is nice app... \n \thttp://www.ksmps.com/" );


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();



        return super.onOptionsItemSelected(item);
    }

    /*@SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.txt_user_settings) {
            Intent settingsIntent = new Intent(this, SettingsActivity.class);
            startActivity(settingsIntent);
            // Handle the camera action
        } else if (id == R.id.txt_user_invite) {
            invite();

        } else if (id == R.id.txt_user_contact_us) {
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", "+91 999 464 7183", null));
            startActivity(intent);

        } else if (id == R.id.logoutTxtView) {
            logout();


        }*/

       /* DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }*/
    private void invite() {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(Intent.EXTRA_TEXT, inviteContent);
        startActivity(Intent.createChooser(sharingIntent, getString(R.string.send_intent_title)));
    }
    private void logout() {
        session.setLoggedIn(false);
        mainEditor.clear();
        mainEditor.commit();
        startActivity(new Intent(this, LoginActivity.class));
        finish();
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }



}


