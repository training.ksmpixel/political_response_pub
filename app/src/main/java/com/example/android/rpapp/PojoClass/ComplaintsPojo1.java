package com.example.android.rpapp.PojoClass;

/**
 * Created by Sparks on 24-02-2018.
 */

public class ComplaintsPojo1 {


    private String sentTo;
    private String Response;
    private String subject;
    private String Complaint_time;
    private String Member_ID;
    private String Content;
    private String Message;
    private String ID;
    private String UserMobileNumber;

    public String getUserMobileNumber() {
        return UserMobileNumber;
    }

    public void setUserMobileNumber(String userMobileNumber) {
        UserMobileNumber = userMobileNumber;
    }

    public ComplaintsPojo1(String subject, String complaint_time, String member_ID, String content, String message, String ID, String sentTo, String response, String comUserMobileNumber) {
        this.subject = subject;
        this.Complaint_time = complaint_time;
        this.Member_ID = member_ID;
        this.Content = content;
        this.Message = message;
        this.ID = ID;

        this.sentTo = sentTo;
        this.Response = response;
        this.UserMobileNumber = comUserMobileNumber;
    }



    public String getSentTo() {
        return sentTo;
    }

    public void setSentTo(String sentTo) {
        this.sentTo = sentTo;
    }

    public String getResponse() {
        return Response;
    }

    public void setResponse(String response) {
        Response = response;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getComplaint_time() {
        return Complaint_time;
    }

    public void setComplaint_time(String complaint_time) {
        Complaint_time = complaint_time;
    }

    public String getMember_ID() {
        return Member_ID;
    }

    public void setMember_ID(String member_ID) {
        Member_ID = member_ID;
    }

    public String getContent() {
        return Content;
    }

    public void setContent(String content) {
        Content = content;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }
}
