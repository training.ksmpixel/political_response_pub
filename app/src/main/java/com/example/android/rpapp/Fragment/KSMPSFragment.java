package com.example.android.rpapp.Fragment;



import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;

import com.android.volley.RequestQueue;
import com.example.android.rpapp.Activity.DocumentAttachmentctivity;
import com.example.android.rpapp.PageTransformAdapter.MessageSectionPageAdapter;
import com.example.android.rpapp.PageTransformationHelper.ZoomOutPageTransformer;
import com.example.android.rpapp.Progress.Progress;
import com.example.android.rpapp.R;
import com.example.android.rpapp.Singleton.SingleTon;
import com.example.android.rpapp.Variables.StaticVariables;


/*import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.android.volley.RequestQueue;
import com.example.android.rpapp.Activity.DocumentAttachmentctivity;
import com.example.android.rpapp.Progress.Progress;
import com.example.android.rpapp.R;
import com.example.android.rpapp.Singleton.SingleTon;
import com.example.android.rpapp.Variables.StaticVariables;*/


/**
 * A simple {@link Fragment} subclass.
 */
public class KSMPSFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener  {

    View menuView;

    StaticVariables staticVariables = new StaticVariables();
    RequestQueue mQueue;
    Progress progress;

    Context mContext;
    SwipeRefreshLayout swipeRefreshLayout;
    RecyclerView recyclerView;
    FloatingActionButton float_btn_text,float_btn_image,float_btn_video,float_btn_add;
    boolean isOpen = false;
    Animation fabOpen, fabClose, rotateForward, rotateBackward;


    public KSMPSFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {

        menuView = inflater.inflate(R.layout.fragment_menu, container, false);

        mContext = getActivity();
        progress = new Progress();
        mQueue = SingleTon.getInstance(getActivity().getApplicationContext()).getRequestQueue();

        recyclerView = menuView.findViewById(R.id.public_frag_recycle_view);
        swipeRefreshLayout = menuView.findViewById(R.id.feed_frag_swipe);
        float_btn_text =menuView.findViewById(R.id.float_btn_text);
        float_btn_image = menuView.findViewById(R.id.float_btn_image);
        float_btn_video = menuView.findViewById(R.id.float_btn_video);
        float_btn_add = menuView.findViewById(R.id.float_btn_add) ;


        fabOpen = AnimationUtils.loadAnimation(mContext, R.anim.fab_open);
        fabClose = AnimationUtils.loadAnimation(mContext, R.anim.fab_close);
        rotateForward = AnimationUtils.loadAnimation(mContext, R.anim.rotate_forward);
        rotateBackward = AnimationUtils.loadAnimation(mContext, R.anim.rotate_backward);

        float_btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent AttachmentIntent = new Intent(mContext, DocumentAttachmentctivity.class);
                startActivity(AttachmentIntent);
            }
        });

        return menuView;
    }

    @Override
    public void onRefresh() {
        OnResume();

    }
    public void OnResume(){

    }






}
