package com.example.android.rpapp.Adapter;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.android.rpapp.PojoClass.ComplaintsPojo;
import com.example.android.rpapp.Progress.Progress;
import com.example.android.rpapp.R;
import com.example.android.rpapp.Singleton.SingleTon;
import com.example.android.rpapp.Variables.StaticVariables;
import com.ms.square.android.expandabletextview.ExpandableTextView;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * Created by Sparks on 24-02-2018.
 */

public class ComplaintsListAdapter extends RecyclerView.Adapter<ComplaintsListAdapter.RunViewHolder> {

    private Context context;
    private List<ComplaintsPojo> complaintsPojoList;
    private SparseBooleanArray mCollapsedStatus;
    LinearLayout rootComplaintView;
    String ComplaintResponse;
    private RequestQueue mQueue;
    StaticVariables staticVariables = new StaticVariables();
    public String COM_RES_URL = staticVariables.COM_RES_URL;
    Progress progress;
    private String REFERENCE_USER_VERIFY = staticVariables.REFERENCE_USER_VERIFY;
    public static final int TIME_OUT = 1000;



    public ComplaintsListAdapter(Context mContext, List<ComplaintsPojo> complaintsPojoList) {
        mCollapsedStatus = new SparseBooleanArray();
        this.context = mContext;
        this.complaintsPojoList =complaintsPojoList;
    }

    @Override
    public RunViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.content_complaints_list, null);
        view.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));
        mQueue = SingleTon.getInstance(context.getApplicationContext()).getRequestQueue();
        progress = new Progress();
        return new RunViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final RunViewHolder holder, final int position) {
        final ComplaintsPojo complaintsPojo = complaintsPojoList.get(position);

        final String ResMessageId = complaintsPojo.getID();

        holder.comSTo.setText("TO                     : " + complaintsPojo.getSentTo());
        holder.comSub.setText("SUBJECT          : " + complaintsPojo.getSubject());
        holder.comId.setText("RECORD ID      : " + complaintsPojo.getID());
        holder.comTime.setText("RECORD DATE : " + complaintsPojo.getComplaint_time());
        holder.comMessage.setText("MESSAGE : " + complaintsPojo.getMessage());
        holder.comMessage1.setVisibility(View.GONE);
        if (!complaintsPojo.getResponse().equals("")) {
            holder.comMessage1.setVisibility(View.VISIBLE);
            holder.comMessage1.setText("RESPONSE : " + complaintsPojo.getResponse());
        }
        holder.expandableTextView.setText(complaintsPojo.getContent(), mCollapsedStatus, position);
        holder.rootComplaintView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                progress.Dialog(context, "Please Wait...");
                mQueue =  SingleTon.getInstance(context.getApplicationContext()).getRequestQueue();

                StringRequest stringRequest = new StringRequest(Request.Method.POST, REFERENCE_USER_VERIFY,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    String JSONResponse = jsonObject.getString("response");
                                    Log.e("JSONResponse res ",""+JSONResponse);
                                    JSONObject jsonObjResponse = new JSONObject(JSONResponse);
                                    final String code = jsonObjResponse.getString("code");
                                    final String message = jsonObjResponse.getString("message");
                                    String result = jsonObjResponse.getString("result");

                                    if (Integer.parseInt(code) == 200){
                                        JSONObject jsonObjResult = new JSONObject(result);
                                        final String  refUserImage = jsonObjResult.getString("userImage");
                                        final String  refUserName = jsonObjResult.getString("varName");
                                        final String  refUserDistrict = jsonObjResult.getString("varDist");
                                        final String  refUserSubDistrict = jsonObjResult.getString("varSub_dist");
                                        final String  refUsermobilenumber = jsonObjResult.getString("varMobile");
                                        new Handler().postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                progress.progressDialog.dismiss();

                                                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                                LayoutInflater inflater = LayoutInflater.from(context);
                                                View dialoglayout = inflater.inflate(R.layout.alertdlg_refernce_number_verify, null);

                                                CircleImageView image =dialoglayout.findViewById(R.id.dialog_imageview);

                                                Picasso.with(context)
                                                        .load(staticVariables.PROFILE_FOLDER+refUserImage)
                                                        .into(image);

                                                TextView nameTxt = dialoglayout.findViewById(R.id.name);
                                                nameTxt.setText(refUserName);
                                                TextView distTxt = dialoglayout.findViewById(R.id.district);
                                                distTxt.setText(refUserDistrict);
                                                TextView subdistTxt = dialoglayout.findViewById(R.id.subdistrict);
                                                subdistTxt.setText(refUserSubDistrict);

                                                builder.setPositiveButton("Call", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {

                                                        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", refUsermobilenumber, null));
                                                        context.startActivity(intent);
                                                        dialog.dismiss();

                                                    }
                                                });
                                                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int i) {
                                                        //  REG_ReferenceNo.setText("");
                                                        dialog.dismiss();

                                                    }
                                                });
                                                builder.setView(dialoglayout);
                                                builder.setCancelable(false);
                                                builder.show();

                                            }
                                        },TIME_OUT);

                                    } else {
                                        new Handler().postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                // REG_ReferenceNo.setText("");
                                                progress.progressDialog.dismiss();
                                                // displayAlert(Integer.parseInt(code),message);
                                            }
                                        },TIME_OUT); }

                                } catch (final Exception e) {
                                    progress.progressDialog.dismiss();
                                    // displayAlert(406,"Server Error");
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(final VolleyError error) {

                            }
                        }){
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        String getCheckRefNumber = complaintsPojo.getUserMobileNumber();

                        params.put("mobile",getCheckRefNumber);

                        return params;
                    }
                };
                mQueue.add(stringRequest);






            }
        });

        holder.share_complaint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Dialog complaintDialog = new Dialog(context);
                complaintDialog.setCancelable(false);
                complaintDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                complaintDialog.setContentView(R.layout.dialog_content_complaints);
                final EditText cdb_complaint_content = complaintDialog.findViewById(R.id.reg_edt_cdb);

                ImageView cdb_dismissBtn = complaintDialog.findViewById(R.id.cdb_btn_dismiss);
                Button cdb_cancel_Btn = complaintDialog.findViewById(R.id.cancel_cdb_txt);
                Button cdb_submit_Btn = complaintDialog.findViewById(R.id.text_cdb_post);


                cdb_dismissBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        complaintDialog.dismiss();
                    }
                });

                cdb_cancel_Btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        complaintDialog.dismiss();
                    }
                });
                cdb_submit_Btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ComplaintResponse = cdb_complaint_content.getText().toString();


                        if (ComplaintResponse.equals("")) {
                            Toast.makeText(context, "Please Type Your Message", Toast.LENGTH_LONG).show();

                        } else {


                            StringRequest stringRequest = new StringRequest(Request.Method.POST, COM_RES_URL,
                                    new Response.Listener<String>() {
                                        @Override
                                        public void onResponse(String response) {
                                            try {
                                                JSONObject jsonObject = new JSONObject(response);
                                                String JSONResponse = jsonObject.getString("response");
                                                Log.e("response ", "" + JSONResponse);
                                                JSONObject jsonObjResponse = new JSONObject(JSONResponse);
                                                String code = jsonObjResponse.getString("code");
                                                Log.e("code ", "" + code);

                                                final String message = jsonObjResponse.getString("message");
                                                Log.e("message ", "" + message);

                                                String result = jsonObjResponse.getString("result");
                                                Log.e("result ", "" + result);


                                                if (code.equals("200")) {

                                                    complaintDialog.dismiss();
                                                }
                                            } catch (JSONException e) {

                                            }
                                        }
                                    },
                                    new Response.ErrorListener() {
                                        @Override
                                        public void onErrorResponse(VolleyError error) {

                                        }
                                    }) {
                                protected Map<String, String> getParams() throws AuthFailureError {
                                    Map<String, String> params = new HashMap<>();
                                    params.put("complaint_id", ResMessageId);
                                    params.put("message", ComplaintResponse);
                                    return params;
                                }
                            };
                            mQueue.add(stringRequest);
                            complaintDialog.dismiss();

                        }
                    }
                });
                complaintDialog.show();
            }
        });
        holder.share_complaint.setVisibility(View.VISIBLE);

    }


    @Override
    public int getItemCount() {
        return complaintsPojoList.size();
    }

    public class RunViewHolder extends RecyclerView.ViewHolder {


        TextView comSub, comId, comTime, comMessage,comMessage1,comSTo;
        ImageView share_complaint, Less;
        ExpandableTextView expandableTextView;
        LinearLayout rootComplaintView;

        public RunViewHolder(View itemView) {
            super(itemView);

            comSTo = itemView.findViewById(R.id.complaint_sub_to);
            comSub = itemView.findViewById(R.id.complaint_sub_name);
            comId = itemView.findViewById(R.id.complaint_id);
            comTime = itemView.findViewById(R.id.compalaint_time);
            comMessage = itemView.findViewById(R.id.complaint_sub_message);
            comMessage1 = itemView.findViewById(R.id.complaint_sub_message2);
            expandableTextView= itemView.findViewById(R.id.expand_text_view);
            share_complaint= itemView.findViewById(R.id.share_complaint);
            rootComplaintView= itemView.findViewById(R.id.rootComplaintView);
        }
    }
}
