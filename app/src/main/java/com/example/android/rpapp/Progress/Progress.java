package com.example.android.rpapp.Progress;

import android.app.ProgressDialog;
import android.content.Context;

import com.example.android.rpapp.R;


public class Progress {

    public ProgressDialog progressDialog;

    public void Dialog(Context context, String message){
        progressDialog = new ProgressDialog(context, R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    public void dialogWithTitle(Context context, String title, String message){
        progressDialog = new ProgressDialog(context, R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setTitle(title);
        progressDialog.setMessage(message);
        progressDialog.show();
    }
}
