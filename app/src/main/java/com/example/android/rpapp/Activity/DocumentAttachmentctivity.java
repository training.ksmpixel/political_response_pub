package com.example.android.rpapp.Activity;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.rpapp.R;

import java.io.IOException;

public class DocumentAttachmentctivity extends AppCompatActivity implements View.OnClickListener {
    EditText edt_heading,edt_content;       ;
    TextView txt_file_pick,txt_file_pick_audio,txt_file_pick_video;
    ImageView img_view_1,img_view_2,img_view_3;
    CheckBox chk_agree;
    Button btn_server_upload;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_document_attachmentctivity);

        edt_heading =(EditText)findViewById(R.id.edt_heading) ;
        edt_content =(EditText)findViewById(R.id.edt_content) ;
        txt_file_pick =(TextView)findViewById(R.id.txt_file_pick) ;
        txt_file_pick_audio =(TextView)findViewById(R.id.txt_file_pick_audio) ;
        txt_file_pick_video =(TextView)findViewById(R.id.txt_file_pick_video) ;
        img_view_1 =(ImageView)findViewById(R.id.img_view_1) ;
        img_view_2 =(ImageView)findViewById(R.id.img_view_2) ;
        img_view_3 =(ImageView)findViewById(R.id.img_view_3) ;
        chk_agree = (CheckBox)findViewById(R.id.chk_agree);
        btn_server_upload = (Button)findViewById(R.id.btn_server_upload);
        chk_agree.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked){
                    btn_server_upload.setEnabled(true);
                }else {
                    btn_server_upload.setEnabled(false);
                }

            }
        });
        btn_server_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(DocumentAttachmentctivity.this,"uploaded succesfully",Toast.LENGTH_SHORT).show();
            }
        });
        txt_file_pick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_GET_CONTENT);
                intent.setType("appliction/pdf");
                startActivityForResult(intent,100);

            }
        });
        txt_file_pick_audio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_GET_CONTENT);
                intent.setType("audio/*");
                startActivityForResult(intent,200);


            }
        });
        txt_file_pick_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_GET_CONTENT);
                intent.setType("video/*");
                startActivityForResult(intent,300);
                }
        });
        img_view_1.setOnClickListener(this);
        img_view_2.setOnClickListener(this);
        img_view_3.setOnClickListener(this);
        }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 100:
                if (resultCode == RESULT_OK) {
                    String FileName = getFileName(data.getData());
                    txt_file_pick.setText(FileName);
                }
                break;
            case 200:
                if (resultCode == RESULT_OK) {
                    String AudioName = getFileName(data.getData());
                    txt_file_pick_audio.setText(AudioName);
                }
                break;
            case 300:
                if (resultCode == RESULT_OK) {
                    String VideoName = getFileName(data.getData());
                    txt_file_pick_video.setText(VideoName);
                }
                break;
            case 400:
                if (resultCode == RESULT_OK) {
                    Uri uri = data.getData();

                    try {
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                        // Log.d(TAG, String.valueOf(bitmap));
                        img_view_1.setImageBitmap(bitmap);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                break;
            case 500:
                if (resultCode == RESULT_OK) {
                    Uri uri = data.getData();

                    try {
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                        // Log.d(TAG, String.valueOf(bitmap));
                        img_view_2.setImageBitmap(bitmap);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                break;
            case 600:
                if (resultCode == RESULT_OK) {
                    Uri uri = data.getData();

                    try {
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                        // Log.d(TAG, String.valueOf(bitmap));
                        img_view_3.setImageBitmap(bitmap);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                break;


        }
    }

    public String getFileName(Uri uri){
        String result = null;
        if(uri.getScheme().equals("content")){
            Cursor cursor = getContentResolver().query(uri,null,null,null,null);
            try{
                if(cursor !=null && cursor.moveToFirst()){
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                    }
            }finally {
                cursor.close();
                }
                }
                if(result == null){
                    result = uri.getPath();
                    int cut = result.lastIndexOf('/');
                    if (cut != -1) {
                        result = result.substring(cut + 1);
                    }
                    }
                    return result;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.img_view_1:
                Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                galleryIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);

                startActivityForResult(galleryIntent, 400);
                break;
              /*  Intent intent = new Intent();
                intent.setType("image/*");
                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), 400);
*/
            case R.id.img_view_2:
                Intent galleryIntent1 = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(galleryIntent1, 500);
                break;
            case R.id.img_view_3:
                Intent galleryIntent2 = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(galleryIntent2, 600);
                break;

        }
        }
        }



