package com.example.android.rpapp.PageTransformAdapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;

import com.example.android.rpapp.Fragment.KSMPSFragment;
import com.example.android.rpapp.Fragment.MessageFragment;
import com.example.android.rpapp.Fragment.PublicFragment;

import java.util.HashMap;
import java.util.Map;

public class MainSectionsPageAdapter extends FragmentPagerAdapter {

    private static int NUM_ITEMS = 3;
    private Map<Integer, String> mFragmentTags;
    private FragmentManager mFragmentManager;

    public MainSectionsPageAdapter(FragmentManager fm) {
        super(fm);
        mFragmentManager = fm;
        mFragmentTags = new HashMap<Integer, String>();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){
            case 0:
                return "PUBLIC";
            case 1:
                return "MESSAGE";
            case 2:
                return "KSMPS";

            default:
                    return null;
        }
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                PublicFragment publicFragment = new PublicFragment();
                return publicFragment;
            case 1:
                MessageFragment messageFragment = new MessageFragment();
                return messageFragment;
            case 2:
                KSMPSFragment ksmpsFragment = new KSMPSFragment();
                return ksmpsFragment;

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return NUM_ITEMS;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Object object = super.instantiateItem(container, position);
        if (object instanceof Fragment){
            Fragment fragment = (Fragment) object;
            String tag = fragment.getTag();
            mFragmentTags.put(position, tag);
        }
        return object;
    }

    public Fragment getFragment(int position){
        Fragment fragment = null;
        String tag = mFragmentTags.get(position);
        if(tag!=null){
            fragment = mFragmentManager.findFragmentByTag(tag);
        }
        return fragment;
    }
}