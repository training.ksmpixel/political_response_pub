package com.example.android.rpapp.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.android.rpapp.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class PublicFragment extends Fragment {

    View PublicView;

    public PublicFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        PublicView = inflater.inflate(R.layout.fragment_public, container, false);

        return PublicView;
    }
}
