package com.example.android.rpapp.Session_SharedPreference;


import android.content.Context;
import android.content.SharedPreferences;

public class Session {
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    Context context;

    String appPref = "ASSIGNMENT_APP_SESSION";

    public Session(Context context){
        this.context = context;
        sharedPreferences = context.getSharedPreferences(appPref, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    public void setLoggedIn(boolean loggedIn){
        editor.putBoolean("LoggedInMode", loggedIn);
        if (loggedIn == false){
            editor.clear();
        }
        editor.commit();
    }

    public boolean loggedIn(){
        return sharedPreferences.getBoolean("LoggedInMode",false);
    }
}
