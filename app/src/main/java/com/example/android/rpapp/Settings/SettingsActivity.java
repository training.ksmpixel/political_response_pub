package com.example.android.rpapp.Settings;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.RingtonePreference;
import android.support.annotation.RequiresApi;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.android.rpapp.Activity.MainActivity;
import com.example.android.rpapp.Progress.Progress;
import com.example.android.rpapp.R;
import com.example.android.rpapp.Session_SharedPreference.Session;
import com.example.android.rpapp.Singleton.SingleTon;
import com.example.android.rpapp.Variables.StaticVariables;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SettingsActivity extends AppCompatPreferenceActivity {

    private Session session;
    SharedPreferences menuSharedPreference;
    SharedPreferences.Editor menuEditor;
    Context mContext;
    Progress progress;
    AlertDialog.Builder dialogBuilder;
    private RequestQueue mQueue;
    StaticVariables staticVariables = new StaticVariables();
    private String CHANGE_PASSWORD_URL = staticVariables.CHANGE_PASSWORD_API_FILE;
    private static final int TIME_OUT = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Typeface typeface = Typeface.createFromAsset(getApplicationContext().getAssets(), "montserrat_regular.ttf");
        session = new Session(getApplicationContext());
        mContext = SettingsActivity.this;

        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        TextView textView = new TextView(SettingsActivity.this);
        textView.setLayoutParams(layoutParams);
        textView.setText("SETTINGS");
        textView.setTextSize(18);
        textView.setTypeface(typeface);
        textView.setGravity(Gravity.LEFT);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            textView.setTextColor(getColor(R.color.colorWhite));
        }


        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setCustomView(textView);

        getFragmentManager().beginTransaction().replace(android.R.id.content, new MainPreferenceFragment()).commit();

    }

    @SuppressLint("ValidFragment")
    public class MainPreferenceFragment extends PreferenceFragment {
        @SuppressLint("CommitPrefEdits")
        @Override
        public void onCreate(final Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.setting_main);
            menuSharedPreference = getApplicationContext().getSharedPreferences(StaticVariables.LOG_PREF, Context.MODE_MULTI_PROCESS);
            menuEditor = menuSharedPreference.edit();
            progress = new Progress();
            dialogBuilder = new AlertDialog.Builder(SettingsActivity.this);

//            bindPreferenceSummaryToValue(findPreference(getString(R.string.key_notifications_ringtone)));

            Preference myPref = findPreference(getString(R.string.key_send_feedback));
            myPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                public boolean onPreferenceClick(Preference preference) {
                    sendFeedback(getActivity());
                    return true;
                }
            });

            /*final SwitchPreference NotificationSound = (SwitchPreference) findPreference("notification_sound");
            NotificationSound.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object o) {
                    if(NotificationSound.isChecked()){
                        menuEditor.putString(StaticVariables.KEY_NOTIFICATION_STATUS,"Disabled");
                        menuEditor.commit();
                        Toast.makeText(getApplicationContext(),"Notification Sound " + menuSharedPreference.getString(StaticVariables.KEY_NOTIFICATION_STATUS,"Null"),Toast.LENGTH_SHORT).show();
                        NotificationSound.setChecked(false);
                    }else {
                        menuEditor.putString(StaticVariables.KEY_NOTIFICATION_STATUS,"Enabled");
                        menuEditor.commit();
                        Toast.makeText(getApplicationContext(),"Notification Sound " + menuSharedPreference.getString(staticVariables.KEY_NOTIFICATION_STATUS,"Null"),Toast.LENGTH_SHORT).show();
                        NotificationSound.setChecked(true);
                    }
                    return true;
                }
            });*/

            Preference changePasswordKey = (Preference) findPreference("change_password_key");
            changePasswordKey.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.M)
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    ChangePassword();
                    return false;
                }
            });
        }
    }

    private boolean isValidPassword(String pass) {
        if (pass != null && pass.length() >= 8) {
            return true;
        }
        return false;
    }

    public void ChangePassword() {
        final Dialog dialog = new Dialog(SettingsActivity.this);
        dialog.setCancelable(false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_content_change_password);

        dialogBuilder = new AlertDialog.Builder(SettingsActivity.this);
        final EditText editCurrentPassword = dialog.findViewById(R.id.edt_current_password);
        final EditText editChangePassword = dialog.findViewById(R.id.edt_change_password);
        final EditText editChangePassword1 = dialog.findViewById(R.id.edt_change_password1);
        final Button btnFPProceed = dialog.findViewById(R.id.change_dlg_btn_proceed);
        final ImageButton imgBtnDismiss = dialog.findViewById(R.id.change_dlg_icon_dismiss);
        final String MobNumber = menuSharedPreference.getString(staticVariables.KEY_USER_MOBILE_NO, "Null");

        imgBtnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        btnFPProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progress.Dialog(SettingsActivity.this, "Authenticating...");

                final String getCurrentPassword = editCurrentPassword.getText().toString();
                final String getPassword = editChangePassword.getText().toString();
                final String getConfirmPassword = editChangePassword1.getText().toString();

                if (!isValidPassword(getPassword)) {
                    progress.progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), "Password must have 8 Characters", Toast.LENGTH_LONG).show();
                } else if (!isValidPassword(getConfirmPassword)) {
                    progress.progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), "Confirm Password must have 8 Characters", Toast.LENGTH_LONG).show();
                } else if (!getPassword.equals(getConfirmPassword)) {
                    progress.progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), "Password and Confirm Password are not same", Toast.LENGTH_LONG).show();
                } else {
                    final InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    assert imm != null;
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                    mQueue = SingleTon.getInstance(getApplicationContext()).getRequestQueue();

                    StringRequest stringRequest = new StringRequest(Request.Method.POST, CHANGE_PASSWORD_URL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        String JSONResponse = jsonObject.getString("response");
                                        JSONObject jsonObjResponse = new JSONObject(JSONResponse);
                                        final String code = jsonObjResponse.getString("code");
                                        final String message = jsonObjResponse.getString("message");
                                        String result = jsonObjResponse.getString("result");
                                        new Handler().postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                dialog.dismiss();
                                                progress.progressDialog.dismiss();
                                                Toast.makeText(getApplicationContext(), " " + message, Toast.LENGTH_LONG).show();
                                            }
                                        }, TIME_OUT);
                                    } catch (final JSONException e) {
                                        new Handler().postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                progress.progressDialog.dismiss();
                                                Toast.makeText(mContext, String.valueOf(e), Toast.LENGTH_LONG).show();
                                            }
                                        }, TIME_OUT);
                                    }
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(final VolleyError error) {
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            progress.progressDialog.dismiss();
                                            Toast.makeText(mContext, String.valueOf(error), Toast.LENGTH_LONG).show();
                                        }
                                    }, TIME_OUT);
                                }
                            }) {
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> params = new HashMap<>();
                            params.put("currentPassword", getCurrentPassword);
                            params.put("newPassword", getPassword);
                            params.put("mobile", MobNumber);
                            return params;
                        }
                    };
                    mQueue.add(stringRequest);
                }
            }
        });
        dialog.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(mContext, MainActivity.class);
                intent.putExtra("viewpager_position", 4);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void sendFeedback(Context context) {
        String body = null;
        try {
            body = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
            body = "\n\n-----------------------------\nPlease don't remove this information\n Device OS: Android \n Device OS version: " +
                    Build.VERSION.RELEASE + "\n App Version: " + body + "\n Device Brand: " + Build.BRAND +
                    "\n Device Model: " + Build.MODEL + "\n Device Manufacturer: " + Build.MANUFACTURER;
        } catch (PackageManager.NameNotFoundException e) {
        }
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("message/rfc822");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"training.ksmpixel@gmail.com"});
        intent.putExtra(Intent.EXTRA_SUBJECT, "Query from Cricket Alpha");
        intent.putExtra(Intent.EXTRA_TEXT, body);
        context.startActivity(intent);/*Intent.createChooser(intent, context.getString(R.string.choose_email_client)));*/
    }

    private void bindPreferenceSummaryToValue(Preference preference) {
        preference.setOnPreferenceChangeListener(sBindPreferenceSummaryToValueListener);

        sBindPreferenceSummaryToValueListener.onPreferenceChange(preference,
                PreferenceManager
                        .getDefaultSharedPreferences(preference.getContext())
                        .getString(preference.getKey(), ""));
    }

    private Preference.OnPreferenceChangeListener sBindPreferenceSummaryToValueListener = new Preference.OnPreferenceChangeListener() {
        @Override
        public boolean onPreferenceChange(Preference preference, Object newValue) {
            String stringValue = newValue.toString();

            if (preference instanceof ListPreference) {
                ListPreference listPreference = (ListPreference) preference;
                int index = listPreference.findIndexOfValue(stringValue);
                preference.setSummary(
                        index >= 0
                                ? listPreference.getEntries()[index]
                                : null);

            } else if (preference instanceof RingtonePreference) {
                if (TextUtils.isEmpty(stringValue)) {
                    preference.setSummary(R.string.pref_ringtone_silent);

                } else {
                    Ringtone ringtone = RingtoneManager.getRingtone(
                            preference.getContext(), Uri.parse(stringValue));
                    //                    menuEditor.putString(StaticVariables.KEY_NOTIFICATION_RINGTONE, stringValue);
                    menuEditor.commit();

                    if (ringtone == null) {
                        preference.setSummary(R.string.summary_choose_ringtone);
                    } else {
                        String name = ringtone.getTitle(preference.getContext());
                        preference.setSummary(name);
                    }
                }
            } else if (preference instanceof EditTextPreference) {
                if (preference.getKey().equals("key_gallery_name")) {
                    preference.setSummary(stringValue);
                }
            } else {
                preference.setSummary(stringValue);
            }
            return true;
        }
    };



    }

