package com.example.android.rpapp.Fragment;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.android.rpapp.R;
import com.example.android.rpapp.Variables.StaticVariables;


/**
 * A simple {@link Fragment} subclass.
 */
public class AboutUsFragment extends Fragment {

    View AboutUsView;
    Context mContext;
    SharedPreferences logSharedPreferences;
    StaticVariables staticVariables = new StaticVariables();
    String UserName;
    public AboutUsFragment() {
        // Required empty public constructor
    }


    @SuppressLint("SetTextI18n")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        AboutUsView =  inflater.inflate(R.layout.fragment_about_us, container, false);

        mContext= getActivity();


        TextView outputText = (TextView)AboutUsView.findViewById(R.id.scrollingtext);

        outputText.setMovementMethod(new ScrollingMovementMethod());

        logSharedPreferences = mContext.getSharedPreferences(staticVariables.LOG_PREF, Context.MODE_PRIVATE);

        UserName = logSharedPreferences.getString(staticVariables.KEY_USER_NAME,"");

        outputText.setText("Welcome "+UserName+". Have a Great Day");
        TextView aboutUsContent = (TextView)AboutUsView.findViewById(R.id.content_about_us);
        aboutUsContent.setText(R.string.aboutParty);

        TextView aboutUsContent_n = (TextView)AboutUsView.findViewById(R.id.content_about_us_n);
        aboutUsContent_n.setText(R.string.aboutParty_n);

        TextView founderTXT = (TextView)AboutUsView.findViewById(R.id.content_about_found);
        founderTXT.setText(R.string.founder);

        TextView careerTXT = (TextView)AboutUsView.findViewById(R.id.content_about_carreer);
        careerTXT.setText(R.string.carreer);

      /*  TextView ControversyTXT = (TextView)AboutUsView.findViewById(R.id.content_about_Controversy);
        ControversyTXT.setText(R.string.Controversy);*/

        TextView pCareerTXT = (TextView)AboutUsView.findViewById(R.id.content_about_pCarrer);
        pCareerTXT.setText(R.string.pCareer);

        return AboutUsView;
    }

}
