package com.example.android.rpapp.PageTransformAdapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;

import com.example.android.rpapp.Fragment.ComplaintsFragment;
import com.example.android.rpapp.Fragment.OthersFragment;
import com.example.android.rpapp.Fragment.ThanksFragment;
import com.example.android.rpapp.Fragment.WishFragment;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Android_Admin on 3/22/2018.
 */

public class MessageSectionPageAdapter extends FragmentPagerAdapter {

    private static int NUM_ITEMS = 4;
    private Map<Integer, String> mFragmentTags;
    private FragmentManager mFragmentManager;

    public MessageSectionPageAdapter(FragmentManager fm) {
        super(fm);
        mFragmentManager = fm;
        mFragmentTags = new HashMap<>();
    }


    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Wishes";
            case 1:
                return "Thanks";
            case 2:
                return "Queries";
            case 3:
                return "Others";
            default:
                return null;
        }
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                WishFragment messageWish1Fragment = new WishFragment();
                return messageWish1Fragment;
            case 1:
                ThanksFragment messageThanksFragment = new ThanksFragment();
                return messageThanksFragment;
            case 2:
                ComplaintsFragment messageFragment = new ComplaintsFragment();
                return messageFragment;
            case 3:
                OthersFragment othersFragment = new OthersFragment();
                return othersFragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return NUM_ITEMS;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Object object = super.instantiateItem(container, position);
        if (object instanceof Fragment) {
            Fragment fragment = (Fragment) object;
            String tag = fragment.getTag();
            mFragmentTags.put(position, tag);
        }
        return object;
    }

    public Fragment getFragment(int position) {
        Fragment fragment = null;
        String tag = mFragmentTags.get(position);
        if (tag != null) {
            fragment = mFragmentManager.findFragmentByTag(tag);
        }
        return fragment;
    }
}
