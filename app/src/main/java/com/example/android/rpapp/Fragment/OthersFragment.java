package com.example.android.rpapp.Fragment;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.android.rpapp.Adapter.ComplaintsListAdapter1;
import com.example.android.rpapp.PojoClass.ComplaintsPojo1;
import com.example.android.rpapp.Progress.Progress;
import com.example.android.rpapp.R;
import com.example.android.rpapp.Singleton.SingleTon;
import com.example.android.rpapp.Variables.StaticVariables;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class OthersFragment extends Fragment  implements SwipeRefreshLayout.OnRefreshListener {
    View ThanksView;

    RecyclerView recyclerView;
    FloatingActionButton addNewComplaintsBtn;
    SharedPreferences logSharedPreferences;

    private ArrayAdapter<CharSequence> ComplaintTypes;
    private ArrayAdapter<CharSequence> ComplaintSentTo;
    SharedPreferences.Editor logEditor;

    StaticVariables staticVariables = new StaticVariables();

    Context mContext;
    String Mob,member_id,name,area,ac,dist;

    SwipeRefreshLayout swipeRefreshLayout;
    private RequestQueue mQueue;

    List<ComplaintsPojo1> complaintsPojoList1;
    ComplaintsListAdapter1 complaintsListAdapter1;

    ComplaintsPojo1 complaintsPojo1;

    String ComplaintContent;
    public static final int TIME_OUT = 3000;
    private String COMPLAINT_URL = staticVariables.COMPLAINT_URL;
    private String COMPLAINT_VIEW_URL = staticVariables.ALL_COMPLAINT_URL;

    Progress progress;

    public OthersFragment() { }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ThanksView = inflater.inflate(R.layout.fragment_others, container, false);

        recyclerView = ThanksView.findViewById(R.id.complaint_frag_ground_recycle_view);
        //addNewComplaintsBtn = ComplaintsView.findViewById(R.id.complaint_frag_float_btn_add);
        mContext = getActivity();

        logSharedPreferences = mContext.getSharedPreferences(staticVariables.LOG_PREF, Context.MODE_PRIVATE);
        logEditor = logSharedPreferences.edit();
        swipeRefreshLayout = ThanksView.findViewById(R.id.feed_frag_swipe);

        progress = new Progress();

        Mob =logSharedPreferences.getString(staticVariables.KEY_USER_MOBILE_NO,"");
        member_id =logSharedPreferences.getString(staticVariables.KEY_USER_MEMBER_ID,"");
        name =logSharedPreferences.getString(staticVariables.KEY_USER_NAME,"");
        area =logSharedPreferences.getString(staticVariables.KEY_USER_AREA,"");
        ac =logSharedPreferences.getString(staticVariables.KEY_USER_AC,"");
        dist =logSharedPreferences.getString(staticVariables.KEY_USER_DIST,"");

        mQueue = SingleTon.getInstance(getActivity().getApplicationContext()).getRequestQueue();

        swipeRefreshLayout.setOnRefreshListener(this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        complaintsPojoList1 = new ArrayList<>();

        complaintsListAdapter1 = new ComplaintsListAdapter1(mContext, complaintsPojoList1);
        recyclerView.setAdapter(complaintsListAdapter1);

        return ThanksView;
    }

    @Override
    public void onResume() {
        super.onResume();
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                getComplaints();
            }
        });
    }

    public void sendFeedback(String CContent, String Subject) {
        String body = null;
        body = CContent+"\n\nRegards,\n"+name+"\n"+ area+" \n"+ ac+ "\n"+dist+"\n ";

        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("message/rfc822");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"training.ksmpixel@gmail.com","azhagar333@gmail.com","rkranjith100@gmail.com"});
        intent.putExtra(Intent.EXTRA_SUBJECT, "Regarding "+Subject);
        intent.putExtra(Intent.EXTRA_TEXT, body);
        mContext.startActivity(intent);
    }

    private void getComplaints() {
        swipeRefreshLayout.setRefreshing(true);
        complaintsPojoList1.clear();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, COMPLAINT_VIEW_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String JSONResponse = jsonObject.getString("response");
                            JSONObject jsonObjResponse = new JSONObject(JSONResponse);
                            String code = jsonObjResponse.getString("code");
                            final String message = jsonObjResponse.getString("message");
                            String result = jsonObjResponse.getString("result");
                            JSONArray jsonArray = new JSONArray(result);
                            JSONObject jsonObjResult = null;
                            if (code.equals("200")) {

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    jsonObjResult = jsonArray.getJSONObject(i);
                                    String ComSub = jsonObjResult.getString("VarCompalintSubject");
                                    String ComTime = jsonObjResult.getString("VarComplaintTime");
                                    String ComMemId = jsonObjResult.getString("VarMemberID");
                                    String ComCont = jsonObjResult.getString("VarComplaintContent");
                                    String ComMsg = jsonObjResult.getString("txtMessage");
                                    String ComID = jsonObjResult.getString("ID");
                                    String ComSentTo = jsonObjResult.getString("varSentTo");
                                    String ComResponse = jsonObjResult.getString("varResponse");
                                    String ComUserMobileNumber = jsonObjResult.getString("varMobile");

                                    complaintsPojo1 = new ComplaintsPojo1(ComSub, ComTime, ComMemId, ComCont, ComMsg, ComID,ComSentTo,ComResponse, ComUserMobileNumber);
                                    complaintsPojoList1.add(complaintsPojo1);
                                }
                                complaintsListAdapter1.notifyDataSetChanged();
                            } else {
                                complaintsListAdapter1.notifyDataSetChanged();
                            }
                        } catch (JSONException e) {
                            complaintsListAdapter1.notifyDataSetChanged();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        complaintsListAdapter1.notifyDataSetChanged();
                    }
                }) {
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("subject","Others");
                return params;
            }
        };
        mQueue.add(stringRequest);
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onRefresh() {
        onResume();
    }
}


